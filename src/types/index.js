// @flow
export type Activity = {
  id: string,
  title: string,
  shortDescription: string,
  longDescription: string,
  price: number,
  currency: string,
  rating: number,
  numberOfRatings: number,
  coverImage: string,
};
