import React from 'react';
import { View, Text } from 'react-native';
import styles, { starSize, starStyle } from './styles';
import CoverImage from './CoverImage';
import RatingStars from '../RatingStars';

function ActivityCard({
  title,
  subtitle,
  rating,
  price,
  imageSrc,
  numberOfRatings,
  style,
}: {
  title: string,
  subtitle: string,
  rating: number,
  price: number,
  imageSrc: ?string,
  numberOfRatings: number,
  style?: any,
}) {
  return (
    <View style={[styles.cardContainer, style]}>
      <CoverImage style={styles.coverImage} imageSrc={imageSrc}>
        <View style={styles.infoContainer}>
          <Text style={styles.titleText}>{title}</Text>
          <Text style={styles.subtitleText}>{subtitle}</Text>
          <View style={styles.ratingAndPriceContainer}>
            <View style={styles.ratingContainer}>
              <RatingStars rating={rating} starSize={starSize} starStyle={starStyle} />
              <Text style={styles.numberOfRatings}>({numberOfRatings})</Text>
            </View>
            <View style={styles.priceContainer}>
              <Text style={styles.priceText}>{price}</Text>
            </View>
          </View>
        </View>
      </CoverImage>
    </View>
  );
}

ActivityCard.defaultProps = {
  style: undefined,
};

export default ActivityCard;
