import { StyleSheet } from 'react-native';

const Colors = {
  cardTitleText: 'white',
  cardSubtitleText: 'rgba(255, 255, 255, 0.8)',
  cardPriceText: 'rgb(2, 182, 210)',
};

export const starSize = 20;
export const starStyle = { marginRight: 3 };

export default StyleSheet.create({
  cardContainer: {
    shadowRadius: 3,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.7,
    height: 390,
    elevation: 3,
    backgroundColor: 'white',
    overflow: 'hidden',
    borderRadius: 25,
  },
  infoContainer: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 10,
    paddingBottom: 7,
    backgroundColor: 'rgba(0,0,0, 0.5)',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: Colors.cardTitleText,
    marginBottom: 5,
  },
  subtitleText: {
    fontSize: 15,
    fontWeight: '500',
    color: Colors.cardSubtitleText,
  },
  ratingAndPriceContainer: {
    flexDirection: 'row',
    alignItems: 'baseline',
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'baseline',
    flex: 1,
  },
  numberOfRatings: {
    color: Colors.cardTitleText,
    marginLeft: 4,
    fontWeight: 'bold',
  },
  priceContainer: {
    backgroundColor: 'white',
    padding: 10,
    paddingLeft: 19,
    paddingRight: 19,
    borderRadius: 15,
  },
  priceText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: Colors.cardPriceText,
  },
  coverImage: {
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
  },
  starStyle: {
    marginRight: 4,
  },
  rating: {},
});
