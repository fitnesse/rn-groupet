// @flow
/* eslint-disable global-require */
import React from 'react';
import { ImageBackground } from 'react-native';

function CoverImage({
  imageSrc,
  style,
  children,
}: {
  imageSrc: ?string,
  style: any,
  children: any,
}) {
  if (!imageSrc) {
    return (
      <ImageBackground
        style={style}
        resizeMode="cover"
        source={require('./assets/placeholder.png')}
      >
        {children}
      </ImageBackground>
    );
  }

  return (
    <ImageBackground style={style} resizeMode="cover" source={{ uri: imageSrc }}>
      {children}
    </ImageBackground>
  );
}

export default CoverImage;
