import React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import ActivityCard from '../index';

storiesOf('ActivityCard', module).add('normal', () => (
  <View style={{ flex: 1 }}>
    <ActivityCard
      title="Santa Elena 1 o 2 noches con desayuno"
      subtitle="Hotel - Restaurante El Mesón"
      rating={4.5}
      price="34 €"
      imageSrc="https://static.pexels.com/photos/20974/pexels-photo.jpg"
      numberOfRatings={240}
    />
  </View>
));
