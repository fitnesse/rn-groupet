// @flow
import React from 'react';
import { FlatList, View } from 'react-native';
import ActivityCard from '../ActivityCard';
import type { Activity } from '../../types';

function renderActivityCard({ item }: { item: Activity }) {
  return (
    <ActivityCard
      title={item.title}
      subtitle={item.shortDescription}
      rating={item.rating}
      numberOfRatings={item.numberOfRatings}
      price={`${item.price} ${item.currency}`}
      imageSrc={item.coverImage}
    />
  );
}

function Separator() {
  return <View style={{ marginTop: 20 }} />;
}

function keyExtractor(item) {
  return item.id;
}

function ActivityCardList({ data, style }: { data: Array<Activity>, style?: any }) {
  return (
    <FlatList
      style={style}
      keyExtractor={keyExtractor}
      ItemSeparatorComponent={Separator}
      renderItem={renderActivityCard}
      data={data}
    />
  );
}

ActivityCardList.defaultProps = {
  style: undefined,
};

export default ActivityCardList;
