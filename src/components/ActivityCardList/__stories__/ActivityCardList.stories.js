// @flow
import React from 'react';
import { storiesOf } from '@storybook/react-native';
import ActivityCardList from '../index';
import type { Activity } from '../../../types';

const exampleActivity = {
  title: 'Santa Elena 1 o 2 noches con desayuno',
  shortDescription: 'Hotel - Restaurante El Mesón',
  longDescription: 'A description longer than the previous one',
  rating: 4.5,
  numberOfRatings: 240,
  price: 34,
  currency: '€',
  coverImage: 'https://static.pexels.com/photos/20974/pexels-photo.jpg',
};

function CreateDummyActivity(): Activity {
  const randomNumber = Math.random() * 10000;
  const result = { ...exampleActivity, id: `${+new Date() + randomNumber}` };

  return result;
}

const allActivities = [];
for (let i = 0; i < 25; i += 1) {
  allActivities.push(CreateDummyActivity());
}

storiesOf('ActivityCardList', module).add('normal', () => (
  <ActivityCardList style={{ padding: 13 }} data={allActivities} />
));
