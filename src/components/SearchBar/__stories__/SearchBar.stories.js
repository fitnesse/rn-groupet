// @flow
import React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import SearchBar from '../index';

const searchBarStyles = {
  width: '100%',
  height: 40,
};

storiesOf('SearchBar', module)
  .addDecorator(story => (
    <View
      style={{
        padding: 20,
        paddingTop: 70,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgb(0, 187, 211)',
      }}
    >
      {story()}
    </View>
  ))
  .add('Empty', () => <SearchBar style={searchBarStyles} />)
  .add('With placeholder', () => <SearchBar placeholder="test" style={searchBarStyles} />)
  .add('With shadow', () => <SearchBar placeholder="test" shadow style={searchBarStyles} />)
  .add('With value', () => <SearchBar style={searchBarStyles} shadow value="Some value" />);
