// @flow
import React from 'react';
import { View, Text, TextInput } from 'react-native';
import styles from './styles';

function SearchBar({
  style,
  placeholder,
  shadow,
  value,
  spellCheck,
}: {
  style?: any,
  shadow?: boolean,
  placeholder?: string,
  value?: string,
  spellCheck?: boolean,
}) {
  return (
    <View style={[styles.container, shadow && styles.searchBarShadow, style]}>
      <Text style={styles.searchIcon}>{'\uf002'}</Text>
      <TextInput
        style={styles.textInput}
        multiline={false}
        value={value}
        spellCheck={spellCheck}
        placeholder={placeholder}
        underlineColorAndroid="transparent"
      />
    </View>
  );
}

SearchBar.defaultProps = {
  style: undefined,
  placeholder: '',
  shadow: false,
  value: undefined,
  spellCheck: false,
};

export default SearchBar;
