// @flow
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 5,
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
  },
  searchIcon: {
    fontFamily: 'FontAwesome',
    fontSize: 24,
    marginLeft: 9,
    color: 'rgba(0,0,0,0.54)',
  },
  textInput: {
    flex: 1,
    height: '400%',
    marginLeft: 15,
    fontSize: 20,
    color: 'rgba(0,0,0,0.7)',
  },
  searchBarShadow: {
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 3,
    shadowOpacity: 0.6,
    elevation: 4,
  },
});
