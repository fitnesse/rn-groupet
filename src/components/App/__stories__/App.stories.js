import React from 'react';
import { View, Text } from 'react-native';
import { storiesOf } from '@storybook/react-native';

storiesOf('App', module).add('Test', () => (
  <View>
    <Text>Hello Button</Text>
  </View>
));
