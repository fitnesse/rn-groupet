// @flow
import React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import RatingStars from '../index';

const CenterView = ({ children }: { children: any }) => (
  <View style={{ alignItems: 'center' }}>{children}</View>
);

storiesOf('RatingStars', module)
  .addDecorator(story => <CenterView>{story()}</CenterView>)
  .add('1 star', () => <RatingStars rating={1.0} />)
  .add('2 stars', () => <RatingStars rating={2.0} />)
  .add('3 star', () => <RatingStars rating={3.0} />)
  .add('4 star', () => <RatingStars rating={4.0} />)
  .add('5 star', () => <RatingStars rating={5.0} />)
  .add('small', () => <RatingStars starSize={20} rating={1.0} />)
  .add('small full', () => <RatingStars starSize={20} rating={5.0} />);
