// @flow
import React from 'react';
import { View } from 'react-native';
import StarRating from 'react-native-star-rating';

function RatingStars({
  rating,
  starSize,
  style,
  starStyle,
}: {
  rating: number,
  style?: any,
  starSize?: number,
  starStyle?: any,
}) {
  return (
    <View style={style}>
      <StarRating
        style={style}
        starSize={starSize}
        starStyle={starStyle}
        starColor="rgb(244,181,3)"
        disabled
        maxStars={5}
        rating={rating}
      />
    </View>
  );
}

RatingStars.defaultProps = {
  style: null,
  starSize: 40,
  starStyle: {},
};

export default RatingStars;
