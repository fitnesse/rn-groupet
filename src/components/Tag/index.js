// @flow
import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

export { default as TagList } from './TagList';

function Tag({ style, children = '' }: { style?: any, children: string }) {
  return (
    <View style={[styles.container, style]}>
      <Text style={styles.tag}>{children}</Text>
    </View>
  );
}

Tag.defaultProps = {
  style: undefined,
};

export default Tag;
