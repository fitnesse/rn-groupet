// @flow
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 15,
    alignItems: 'center',
  },
  tag: {
    backgroundColor: 'transparent',
    color: 'rgb(74,74,74)',
    fontSize: 18,
    margin: 9,
  },
  tagListContainer: {
    flexDirection: 'row',
    overflow: 'scroll',
  },
  TagListTagSpacing: {
    marginRight: 6,
  },
});
