// @flow
import React from 'react';
import { View } from 'react-native';
import Tag from './index';
import styles from './styles';

function TagList({ style, values }: { style?: any, values: Array<string> }) {
  return (
    <View style={[styles.tagListContainer, style]}>
      {values.map(value => (
        <Tag key={value} style={styles.TagListTagSpacing}>
          {value}
        </Tag>
      ))}
    </View>
  );
}

TagList.defaultProps = {
  style: undefined,
};

export default TagList;
