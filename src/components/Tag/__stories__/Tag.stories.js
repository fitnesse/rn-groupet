// @flow
import React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import Tag, { TagList } from '../index';
import SearchBar from '../../SearchBar';

storiesOf('Tag', module)
  .addDecorator(story => (
    <View
      style={{
        padding: 20,
        paddingTop: 70,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgb(0, 187, 211)',
      }}
    >
      {story()}
    </View>
  ))
  .add('Single', () => <Tag style={{ width: '50%' }}>Fechas</Tag>)
  .add('List', () => <TagList values={['Fechas', 'Escapadas', 'Filtros']} />)
  .add('With Search Bar', () => (
    <View>
      <SearchBar shadow style={{ marginBottom: 10 }} />
      <TagList values={['Fechas', 'Huéspedes', 'Escapadas', 'Filtros']} />
    </View>
  ));
